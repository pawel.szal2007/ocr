exports.formatData = (data) => {
  const values = ["1.", "2.", "3.", "4.", "5.", "6.", "7.", "8.", "9.", "10."];

  let formattedData = {};

  for (const page of data.pages) {
    for (const block of page.words) {
      if (values.indexOf(block.word) > -1) {
        if (typeof formattedData[block.word] != "undefined") {
          continue;
        }

        const blocks = require("./middleware/blocks");
        const valueBlock = blocks.findBlockBelow(block, page.words);

        if (valueBlock) {
          formattedData[block.word] = "";
          formattedData[block.word] = valueBlock.word;
          // if(["1.", "2.", "4.", "8.", "10."].includes(block.word)){
          let additionalBlocks = blocks.getAdditionalBlocks(
            valueBlock,
            page.words,
            page.X_WIDTH
          );
          for (const additionalBlock of additionalBlocks) {
            formattedData[block.word] += " " + additionalBlock.word;
          }
          formattedData[block.word] = formattedData[block.word].trim();
          // }
        }
      }
    }
  }
  const mappedData = mapData(formattedData);
  if (mappedData.given_name && mappedData.given_name.split(" ").length > 1) {
    const names = mappedData.given_name.split(" ");
    mappedData.given_name = names[0];
    mappedData.middle_name = names[1];
  }
  mappedData.source = "passport_document_scan";
  mappedData.loa = "medium";
  // const dataObj = convertToBigQuerySchema(userId, mappedData, userDataChecked);
  return mappedData;
};

const mapData = (obj) => {
  const mappedObj = {};
  obj["1."] && (mappedObj["name"] = obj["1."]);
  obj["2."] && (mappedObj["given_name"] = obj["2."]);
  obj["3."] && (mappedObj["nationality"] = obj["3."]);
  obj["4."] && (mappedObj["date_of_birth"] = obj["4."]);
  obj["5."] && (mappedObj["national_id"] = obj["5."]);
  obj["6."] && (mappedObj["gender"] = obj["6."]);
  obj["7."] && (mappedObj["place_of_birth"] = obj["7."]);
  obj["8."] && (mappedObj["issued_date"] = obj["8."]);
  obj["9."] && (mappedObj["issuer_name"] = obj["9."]);
  obj["10."] && (mappedObj["valid_date"] = obj["10."]);

  return mappedObj;
};
