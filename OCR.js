const Storage = require("@google-cloud/storage");
const storage = new Storage.Storage();
const vision = require("@google-cloud/vision");
const client = new vision.ImageAnnotatorClient();
var path = require("path");

// frontend-test-presentation   ps-frontend-test
const bucketName = "ps-frontend-test";

exports.saveData = (fileName, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      await storage
        .bucket(`gs://${bucketName}/`)
        .file(fileName + ".json")
        .save(JSON.stringify(data));
      resolve();
    } catch (err) {
      reject(err);
    }
  });
};

const documentFullTextDetection = (format, fileName) => {
  return new Promise(async (resolve, reject) => {
    try {
      let prefix = "";
      for (let i = 0; i < fileName.split("/").length - 1; i++) {
        prefix += `${fileName.split("/")[i]}/`;
      }
      const gcsUri = `gs://${bucketName}/${fileName}`;
      const today = new Date();
      const isoFormat = today.toISOString();
      const ms = today.getTime();
      const gcsDestinationUri = `gs://${bucketName}/${prefix}ocr_tmp_${isoFormat}__${ms}__`;
      // console.log({gcsUri});
      // console.log({gcsDestinationUri});
      let mimeType = "";

      if (format === "pdf") {
        mimeType = "application/pdf";
      } else {
        mimeType = "image/tiff";
      }

      const inputConfig = {
        mimeType: mimeType,
        gcsSource: {
          uri: gcsUri,
        },
      };

      const outputConfig = {
        gcsDestination: {
          uri: gcsDestinationUri,
        },
      };

      const features = [
        {
          type: "DOCUMENT_TEXT_DETECTION",
        },
      ];

      const request = {
        requests: [
          {
            inputConfig: inputConfig,
            features: features,
            outputConfig: outputConfig,
          },
        ],
      };

      const [operation] = await client.asyncBatchAnnotateFiles(request);
      const [filesResponse] = await operation.promise();
      const destinationUri =
        filesResponse.responses[0].outputConfig.gcsDestination.uri;

      [files] = await storage
        .bucket(`gs://${bucketName}/`)
        .getFiles({ prefix: `${prefix}ocr_tmp_${isoFormat}__${ms}__` });
      for (const file of files) {
        const fileName = file.name;
        // console.log('Reading File ' + fileName);
        var download = await storage
          .bucket(`gs://${bucketName}/`)
          .file(fileName)
          .createReadStream();
        var buf = "";
        await download
          .on("data", function (d) {
            buf += d;
          })
          .on("end", async () => {
            const response = JSON.parse(buf);
            // console.log(response);
            let data = readAllWords(response, format);
            // console.log('deleting file: ', `gs://${bucketName}/${fileName}`);
            await storage.bucket(`gs://${bucketName}/`).file(fileName).delete();
            data ? resolve(data) : reject("Brak danych");
          });
      }
    } catch (err) {
      reject(err);
    }
  });
};

const readAllWords = (response, format) => {
  let allData = {
    pages: [],
  };
  for ([page_index, page_response] of response.responses.entries()) {
    const annotation = page_response.fullTextAnnotation;

    let sentence = "";
    let X_MIN = Number.POSITIVE_INFINITY;
    let X_MAX = null;
    let x_left = Number.POSITIVE_INFINITY;
    let y_up = Number.POSITIVE_INFINITY;
    let x_right = null;
    let y_down = null;

    allData.pages.push({
      page_number: page_index + 1,
      X_WIDTH: null,
      words: [],
    });

    annotation.pages.forEach((page) => {
      for (let [index, block] of page.blocks.entries()) {
        for (let paragraph of block.paragraphs) {
          for (let word of paragraph.words) {
            // page_index === 1 ? console.log(word) : console.log();
            for (let symbol of word.symbols) {
              sentence += symbol.text;
            }

            let vertices;
            if (format === "pdf") {
              vertices = word.boundingBox.normalizedVertices;
            } else vertices = word.boundingBox.vertices;

            for (const vertice of vertices) {
              if (vertice.x < X_MIN) {
                X_MIN = vertice.x;
              }
              if (vertice.x > X_MAX) {
                X_MAX = vertice.x;
              }

              if (vertice.x < x_left) {
                x_left = vertice.x;
              }

              if (vertice.x > x_right) {
                x_right = vertice.x;
              }

              if (vertice.y < y_up) {
                y_up = vertice.y;
              }

              if (vertice.y > y_down) {
                y_down = vertice.y;
              }
            }

            allData.pages[page_index].words.push({
              word: sentence,
              vertices: {
                x_left: x_left,
                x_right: x_right,
                y_up: y_up,
                y_down: y_down,
              },
            });

            sentence = "";
            x_left = Number.POSITIVE_INFINITY;
            y_up = Number.POSITIVE_INFINITY;
            x_right = null;
            y_down = null;
          }
        }
      }
    });

    let X_WIDTH = X_MAX - X_MIN;
    allData.pages[page_index].X_WIDTH = X_WIDTH;
  }
  return allData;
};

exports.process = (fileName) => {
  return new Promise(async (resolve, reject) => {
    if (path.extname(fileName) === ".pdf") {
      resolve(
        await documentFullTextDetection("pdf", fileName).catch((err) => {
          reject(err);
        })
      );
    } else if (path.extname(fileName) === ".tiff") {
      resolve(
        await documentFullTextDetection("tiff", fileName).catch((err) => {
          reject(err);
        })
      );
    } else {
      reject(
        new Error(
          "Nieprawidlowy format pliku wejsciowego. Obslugiwane formaty: pdf oraz tiff"
        )
      );
    }
  });
};

exports.uploadData = (
  bucketName = "ps-frontend-test",
  userId,
  service,
  data
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const today = new Date();
      const ms = today.getTime();
      let buff = new Buffer(`${service}_${today}_${ms}`);
      let name = buff.toString("base64");
      const path = `${userId}/json_data/${service}`;

      await storage
        .bucket(`gs://${bucketName}/`, {
          // user id a potem sciezka z jsonami i źródłem odpowiednim
          destination: path,
        })
        .file(`${path}/${name}.json`)
        .save(JSON.stringify(data), {
          timestamp: ms,
        });
      resolve(
        encodeURI(
          `https://storage.cloud.google.com/${bucketName}/${path}/${name}.json`
        )
      );
    } catch (err) {
      reject(err);
    }
  });
};
