exports.formatData = (data) => {
  const values = [
    "NAZWISKO",
    "IMIONA",
    "DATA",
    "IDENTITY",
    "EXPIRY",
    "SEX",
    "NUMER",
    "ISSUE",
    "ORGAN",
    "OBYWATELSTWO",
    "MIEJSCE",
  ];

  let formattedData = {};

  for (const page of data.pages) {
    for (const block of page.words) {
      if (values.indexOf(block.word) > -1) {
        let suffix = "";
        if (["IMIONA", "NAZWISKO", "DATA", "NUMER"].includes(block.word)) {
          if (
            [
              "RODOWE",
              "RODZICÓW",
              "WYDANIA",
              "URODZENIA",
              "WAŻNOŚCI",
              "PESEL",
            ].includes(page.words[page.words.indexOf(block) + 1].word)
          ) {
            suffix = " " + page.words[page.words.indexOf(block) + 1].word;
          }
        }

        if (typeof formattedData[block.word + suffix] != "undefined") {
          continue;
        }

        const blocks = require("./middleware/blocks");
        const valueBlock = blocks.findBlockBelow(block, page.words);

        if (
          [
            "NAZWISKO",
            "IMIONA",
            "SEX",
            "ORGAN",
            "OBYWATELSTWO",
            "MIEJSCE",
          ].includes(block.word)
        ) {
          if (!/^[a-zA-ZłŁąĄćĆęĘńŃóÓśŚ]+$/.test(valueBlock.word)) {
            continue;
          }
        }

        if (valueBlock) {
          let field;
          if (block.word === "OBYWATELSTWO") {
            field = "nationality";
          } else if (block.word === "IDENTITY") {
            field = "document_number";
          } else if (block.word === "EXPIRY") {
            field = "valid_date";
          } else if (block.word === "SEX") {
            field = "gender";
          } else if (block.word === "ORGAN") {
            field = "issuer_name";
          } else if (block.word === "ISSUE") {
            field = "issued_date";
          } else if (block.word === "MIEJSCE") {
            field = "place_of_birth";
          } else field = block.word + suffix;

          if (field === "NUMER PESEL") {
            field = "national_id";
          } else if (field === "IMIONA") {
            field = "given_name";
          } else if (field === "IMIONA RODZICÓW") {
            field = "parents_given_names";
          } else if (field === "NAZWISKO") {
            field = "name";
          } else if (field === "NAZWISKO RODOWE") {
            field = "family_name";
          } else if (field === "DATA URODZENIA") {
            field = "date_of_birth";
          } else if (field === "DATA WYDANIA") {
            field = "issued_date";
          }

          formattedData[field] = "";
          formattedData[field] = valueBlock.word;

          if (
            ["IMIONA", "NAZWISKO", "ORGAN", "IDENTITY", "MIEJSCE"].includes(
              block.word
            )
          ) {
            let additionalBlocks = blocks.getAdditionalBlocks(
              valueBlock,
              page.words,
              page.X_WIDTH
            );
            for (const additionalBlock of additionalBlocks) {
              formattedData[field] += " " + additionalBlock.word;
            }
            formattedData[field] = formattedData[field].trim();
          }

          if (block.word === "IDENTITY") {
            if (
              formattedData[field].length != 9 &&
              formattedData[field].length != 10
            ) {
              delete formattedData[field];
              continue;
            }
          }
          if (block.word === "NUMER") {
            if (formattedData[field].length != 11) {
              delete formattedData[field];
              continue;
            }
          }
          if (block.word === "DATA" || block.word === "DATE") {
            if (formattedData[field].length != 10) {
              delete formattedData[field];
              continue;
            }
          }
        }
      }
    }
  }
  formattedData.document_name = "identity_card";
  if (
    formattedData.given_name &&
    formattedData.given_name.split(" ").length > 1
  ) {
    const names = formattedData.given_name.split(" ");
    formattedData.given_name = names[0];
    formattedData.middle_name = names[1];
  }
  formattedData.source = "id_document_scan";
  formattedData.loa = "medium";

  return formattedData;
};
