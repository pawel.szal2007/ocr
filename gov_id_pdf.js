exports.formatData = (data) => {
  let formattedData = {};

  for (const page of data.pages) {
    for (const block of page.words) {
      const word = block.word;

      // numer pesel
      if (
        word === "Numer" &&
        page.words[page.words.indexOf(block) + 1].word === "PESEL" &&
        typeof formattedData.national_id === "undefined"
      ) {
        formattedData.national_id =
          page.words[page.words.indexOf(block) + 2].word;
      }

      // nazwisko/nazwisko rodowe
      if (
        word === "Nazwisko" &&
        page.words[page.words.indexOf(block) + 1].word != "rodowe" &&
        page.words[page.words.indexOf(block) + 1].word != "-" &&
        typeof formattedData.name === "undefined"
      ) {
        formattedData.name = page.words[page.words.indexOf(block) + 1].word;
      } else if (
        word === "Nazwisko" &&
        page.words[page.words.indexOf(block) + 1].word === "rodowe" &&
        page.words[page.words.indexOf(block) + 2].word != "matki" &&
        typeof formattedData.family_name === "undefined"
      ) {
        formattedData.family_name =
          page.words[page.words.indexOf(block) + 2].word;
      }

      // imię (imiona)
      if (
        word === "Imię" &&
        page.words[page.words.indexOf(block) + 1].word === "(imiona)" &&
        typeof formattedData.given_name === "undefined"
      ) {
        // pierwsze imię
        formattedData.given_name =
          page.words[page.words.indexOf(block) + 2].word;
        // drugie imie
        page.words[page.words.indexOf(block) + 3].word != "Imię" &&
          (formattedData.middle_name =
            page.words[page.words.indexOf(block) + 3].word);
      }

      // imiona rodziców
      let mother_name, father_name;
      // imię ojca
      if (
        word === "Imię" &&
        page.words[page.words.indexOf(block) + 1].word === "ojca" &&
        typeof formattedData.parents_given_names === "undefined"
      ) {
        father_name = page.words[page.words.indexOf(block) + 2].word;
      }
      // imię matki
      if (
        word === "Imię" &&
        page.words[page.words.indexOf(block) + 1].word === "matki" &&
        typeof formattedData.parents_given_names === "undefined"
      ) {
        mother_name = page.words[page.words.indexOf(block) + 2].word;
      }
      if (
        (mother_name || father_name) &&
        mother_name != undefined &&
        father_name != undefined
      ) {
        formattedData.parents_given_names = `${mother_name} ${father_name}`;
      }

      // data i miejsce urodzenia
      if (
        word === "Data" &&
        page.words[page.words.indexOf(block) + 1].word === "i" &&
        page.words[page.words.indexOf(block) + 2].word === "miejsce" &&
        page.words[page.words.indexOf(block) + 3].word === "urodzenia" &&
        typeof formattedData.date_of_birth === "undefined" &&
        typeof formattedData.place_of_birth === "undefined"
      ) {
        // data urodzenia to pierwszy nastepny blok
        formattedData.date_of_birth =
          page.words[page.words.indexOf(block) + 4].word;
        // miejsce urodzenia, az do Płeć
        for (let i = 5; i > 0 && i < 100; i++) {
          if (page.words[page.words.indexOf(block) + i].word != "Płeć") {
            formattedData.place_of_birth =
              page.words[page.words.indexOf(block) + i].word + " ";
          } else break;
        }
      }

      // płeć
      if (word === "Płeć" && typeof formattedData.gender === "undefined") {
        formattedData.gender = page.words[page.words.indexOf(block) + 1].word;
      }

      // narodowość
      if (
        word === "Obywatelstwo" &&
        typeof formattedData.nationality === "undefined"
      ) {
        formattedData.nationality =
          page.words[page.words.indexOf(block) + 1].word;
      }

      // szukam nazwy "Dowod osobisty"
      let identity_card_page_index;
      if (
        word === "Dowód" &&
        page.words[page.words.indexOf(block) + 1].word === "osobisty" &&
        typeof formattedData.document_number === "undefined"
      ) {
        identity_card_page_index = data.pages.indexOf(page);
        // numer dowodu to dwa kolejne bloki
        formattedData.document_number =
          page.words[page.words.indexOf(block) + 2].word +
          page.words[page.words.indexOf(block) + 3].word;
      }

      // jak mam index strony to znaczy ze to jest strona z dowodem i tam szukam dodanych
      if (identity_card_page_index && identity_card_page_index != undefined) {
        if (
          word === "Data" &&
          page.words[page.words.indexOf(block) + 1].word === "wydania" &&
          page.words[page.words.indexOf(block) + 2].word ===
            "(personalizacji)" &&
          typeof formattedData.issued_date === "undefined"
        ) {
          formattedData.issued_date =
            page.words[page.words.indexOf(block) + 3].word;
        }

        if (
          word === "Data" &&
          page.words[page.words.indexOf(block) + 1].word === "ważności" &&
          page.words[page.words.indexOf(block) + 2].word === "dowodu" &&
          typeof formattedData.valid_date === "undefined"
        ) {
          // data waznosci dowodu i warunek ze jest 10 znakow
          if (page.words[page.words.indexOf(block) + 3].word.length === 10) {
            formattedData.valid_date =
              page.words[page.words.indexOf(block) + 3].word;
          }
        }

        if (
          typeof formattedData.issuer_name === "undefined" &&
          word === "Organ" &&
          page.words[page.words.indexOf(block) + 1].word === "który" &&
          page.words[page.words.indexOf(block) + 2].word === "wydał" &&
          page.words[page.words.indexOf(block) + 3].word === "dowód"
        ) {
          // organ któy wydał dowod
          for (let i = 4; i > 0 && i < 100; i++) {
            if (page.words[page.words.indexOf(block) + i].word != "Status") {
              formattedData.issuer_name =
                page.words[page.words.indexOf(block) + i].word + " ";
            } else break;
          }
        } else if (
          typeof formattedData.issuer_name === "undefined" &&
          word === "Organ" &&
          page.words[page.words.indexOf(block) + 1].word === "," &&
          page.words[page.words.indexOf(block) + 2].word === "który" &&
          page.words[page.words.indexOf(block) + 3].word === "wydał" &&
          page.words[page.words.indexOf(block) + 4].word === "dowód"
        ) {
          // organ który wydał dowod
          for (let i = 5; i > 0 && i < 100; i++) {
            if (page.words[page.words.indexOf(block) + i].word != "Status") {
              formattedData.issuer_name =
                page.words[page.words.indexOf(block) + i].word + " ";
            } else break;
          }
        }
      }
    }
  }
  formattedData.document_name = "identity_card";

  formattedData.source = "gov_id_pdf";
  formattedData.loa = "medium";

  return formattedData;
};
