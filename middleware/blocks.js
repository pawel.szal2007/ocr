exports.findBlockBelow = (block, blocks) => {
  let lowestVectorLength = Number.POSITIVE_INFINITY;
  let nearestBlock = null;
  for (const value_block of blocks) {
    if (value_block.word === block.word) {
      continue;
    }

    let leftVector = Math.sqrt(
      Math.pow(
        Math.abs(value_block.vertices.x_left - block.vertices.x_left),
        2
      ) +
        Math.pow(Math.abs(value_block.vertices.y_up - block.vertices.y_down), 2)
    );
    let rightVector = Math.sqrt(
      Math.pow(
        Math.abs(value_block.vertices.x_right - block.vertices.x_right),
        2
      ) +
        Math.pow(Math.abs(value_block.vertices.y_up - block.vertices.y_down), 2)
    );

    if (
      (value_block.vertices.y_up + value_block.vertices.y_down) / 2 >=
        block.vertices.y_down &&
      (leftVector < lowestVectorLength || rightVector < lowestVectorLength)
    ) {
      lowestVectorLength = Math.min(leftVector, rightVector);
      nearestBlock = value_block;
    }
  }
  return nearestBlock;
};

exports.getAdditionalBlocks = (nearestBlock, blocks, x_width) => {
  let foundBlocks = [];
  for (const blockOnTheSameLevel of blocks) {
    if (blockOnTheSameLevel.word === nearestBlock.word) {
      continue;
    }
    if (
      Math.abs(
        blockOnTheSameLevel.vertices.x_left - nearestBlock.vertices.x_right
      ) <
        0.03 * x_width &&
      (blockOnTheSameLevel.vertices.y_up +
        blockOnTheSameLevel.vertices.y_down) /
        2 >
        nearestBlock.vertices.y_up &&
      (blockOnTheSameLevel.vertices.y_up +
        blockOnTheSameLevel.vertices.y_down) /
        2 <
        nearestBlock.vertices.y_down
    ) {
      foundBlocks.push(blockOnTheSameLevel);
      nearestBlock = blockOnTheSameLevel;
    }
  }
  return foundBlocks;
};
