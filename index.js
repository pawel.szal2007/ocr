(async () => {
  let filePath;
  let fileType;
  let userId;
  // let userDataChecked;

  for (const parameter of process.argv) {
    parameter.includes("filePath") && (filePath = parameter.split("=")[1]);
    parameter.includes("fileType") && (fileType = parameter.split("=")[1]);
    parameter.includes("user_id") && (userId = parameter.split("=")[1]);
    // parameter.includes("data") && (userDataChecked = parameter.split("=")[1]);
  }

  if (!filePath) throw new Error("no filePath parameter");
  if (!fileType) throw new Error("no fileType parameter");
  if (!userId) throw new Error("no user_id parameter");

  // console.log({ filePath }, { fileType }, { userId });
  try {
    const OCR = require("./OCR");
    const data = await OCR.process(filePath);
    let formattedData;
    if (fileType === "identity_card") {
      const identityCard = require("./identityCard");
      formattedData = identityCard.formatData(data);
    } else if (fileType === "passport") {
      const passport = require("./passport");
      formattedData = passport.formatData(data);
    } else if (fileType === "gov_id_pdf") {
      const gov_id_pdf = require("./gov_id_pdf");
      formattedData = gov_id_pdf.formatData(data);
    } else throw new Error(`fileType ${fileType} not recognized`);

    // save to bucket storage
    await OCR.uploadData("ps-frontend-test", userId, fileType, formattedData);

    if (formattedData) {
      console.log(JSON.stringify(formattedData));
    } else {
      throw new Error(
        "formatted data error - data was not formatted correctly"
      );
    }
  } catch (err) {
    console.log(err);
  }
})();
